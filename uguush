#!/bin/sh
#
# uguush - command-line uploader for uguu.se and others
#

## CONFIGURATION

# colors
if [ -x "$(command -v tput)" ]; then
	c0="$(tput sgr0)"
	c1="$(tput setaf 1)"
	c2="$(tput setaf 2)"
fi

# filename to use for screenshots
localTemp="$(mktemp)"

# screenshot functions
capFullscreen() {
	localFile="${localTemp}.png"
	mv "${localTemp}" "${localFile}"
	$screenshotFullscreen "${localFile}"
}

capSelection() {
	localFile="${localTemp}.png"
	mv "${localTemp}" "${localFile}"
	$screenshotSelection "${localFile}"
}

capWindow() {
	localFile="${localTemp}.png"
	mv "${localTemp}" "${localFile}"
	$screenshotWindow $(xdotool getactivewindow) "${localFile}"
}

# delay
delaySeconds='0'

# hosts and shorteners
host='uguu'
shortener=
hosts='uguu teknik 0x0 ptpb mixtape lewd fiery doko'
shorteners='waaai 0x0'

## FUNCTIONS

depends() {
	if [ ! -x "$(command -v curl)" ]; then
		echo "Checking for curl... [${c1}FAILED${c0}]" >&2
		echo 'curl not found.' >&2
		exit 1
	fi
}

usage() {
	cat << EOF

uguush - upload to various file hosts

Usage:
	$(basename "${0}") [options]

Options:
	-d		Delay the screenshot by the specified number of seconds.
	-f		Take a fullscreen screenshot.
	-h		Show this help message.
	-o		Select a host to use. Can be uguu, teknik, 0x0, ptpb, mixtape, lewd, fiery or doko.
	-p <path>	Custom path to save the image to. Saves the image as "%Y-%m-%d %H-%M-%S.png".
	-s		Take a selection screenshot.
	-u <file>	Upload a file.
	-x		Do not notify dbus, update the log, or modify the clipboard.
	-w		Take a screenshot of the current window.
	-S		Select a shortener to use. Can be waaai or 0x0.
	-l <url>	Upload the file at the provided URL.

EOF
}

delay() {
	i="$delaySeconds"
	while [ "$i" -gt 0 ]; do
		echo "${i}..."
		sleep 1
		i="$((i-1))"
	done
}

screenshot() {
	if [ -x "$(command -v maim)" ]; then
		screenshotFullscreen="maim -u"
		screenshotSelection="maim -us"
		screenshotWindow="maim -ui $(xdotool getactivewindow)"
	elif [ -x "$(command -v scrot)" ]; then
		screenshotFullscreen="scrot"
		screenshotSelection="scrot -s"
		screenshotWindow="scrot -u"
	elif [ "${doFullscreen}" ] || [ "${doSelection}" ] || [ "${doWindow}" ]; then 
		echo "Checking for a screenshot utility... [${c1}FAILED${c0}]" >&2
		echo "could not find a screenshot utility, please install 'maim' or 'scrot'" >&2
		exit 1
	fi

	if [ "${doFullscreen}" ]; then
		capFullscreen
	elif [ "${doSelection}" ]; then
		capSelection > /dev/null 2>&1
		if [ ! -s "${localFile}" ]; then
			$(rm "${localFile}" 2> /dev/null)
			exit
		fi
	elif [ "${doWindow}" ]; then
		capWindow
	elif [ "${doURL}" ]; then
		if [ -f "/usr/share/mime/globs" ]; then
			urlExtension="$(curl -sf --head "${remoteURL}" | grep 'Content-Type: ' | head -1 | grep -Po '(?<=\ )[^\;]*')"
			urlExtension="$(echo "${urlExtension}" | sed -e "s/\\r//")"
			urlExtension="$(cat /usr/share/mime/globs | grep "${urlExtension}" | sort -r | head -1 | grep -Po '(?<=\.)[^\n]*')"
		else
			urlExtension="$(basename ${remoteURL})"
			urlExtension=${urlExtension#*.}
		fi
		localFile="${localTemp}.${urlExtension}"
		$(curl -sf "${remoteURL}" > "${localFile}")
	fi
}

upload() {
	i="1"
	while [ "$i" -lt 4 ]; do
		printf "Try #${i}... "
		i="$((i+1))"

		case "${host}" in
			uguu) hostURL='https://uguu.se/api.php?d=upload-tool' ;;
			teknik) hostURL='https://api.teknik.io/v1/Upload' ;;
			0x0) hostURL='https://0x0.st/' ;;
			ptpb) hostURL='https://ptpb.pw/' ;;
			mixtape) hostURL='https://mixtape.moe/upload.php' ;;
			lewd) hostURL='https://lewd.se/api.php?d=upload-tool' ;;
			fiery) hostURL='https://safe.fiery.me/api/upload' ;;
			doko) hostURL='https://doko.moe/upload.php' ;;
		esac

		case "${shortener}" in
			waaai) shortenerURL='https://api.waa.ai/shorten' ;;
			0x0) shortenerURL='http://0x0.st/' ;;
			ptpb) shortenerURL='https://ptpb.pw/u' ;;
		esac

		if [ "${host}" = 'uguu' ]; then
			uploadResult="$(curl -sf -F file="@${localFile}" "${hostURL}")"
		elif [ "${host}" = 'teknik' ]; then
			uploadResult="$(curl -sf -F file="@${localFile}" "${hostURL}")"
			uploadResult="${uploadResult##*url\":\"}"
			uploadResult="${uploadResult%%\"*}"
		elif [ "${host}" = '0x0' ]; then
			uploadResult="$(curl -sf -F file="@${localFile}" "${hostURL}")"
		elif [ "${host}" = 'mixtape' ]; then
			uploadResult="$(curl -sf -F files[]="@${localFile}" "${hostURL}")"
			uploadResult="$(echo "${uploadResult}" | grep -Po '"url":"[A-Za-z0-9]+.*?"' | sed 's/"url":"//;s/"//')"
			uploadResult="$(echo "${uploadResult}" | sed 's/\\//g')"
		elif [ "${host}" = 'ptpb' ]; then
			uploadResult="$(curl -sf -F c="@${localFile}" "${hostURL}")"
			uploadResult="${uploadResult##*url: }"
			uploadResult="${uploadResult%%$'\n'*}"
		elif [ "${host}" = 'lewd' ]; then
			uploadResult="$(curl -sf -F file="@${localFile}" "${hostURL}")"
		elif [ "${host}" = 'fiery' ]; then
			uploadResult="$(curl -sf -F files[]="@${localFile}" "${hostURL}")"
			uploadResult="$(echo "${uploadResult}" | grep -Po '"url":"[A-Za-z0-9]+.*?"' | sed 's/"url":"//;s/"//')"
		elif [ "${host}" = 'doko' ]; then
			uploadResult="$(curl -sf -F files[]="@${localFile}" "${hostURL}")"
			uploadResult="$(echo "${uploadResult}" | grep -Po '"url":"[A-Za-z0-9]+.*?"' | sed 's/"url":"//;s/"//')"
			uploadResult="$(echo "${uploadResult}" | sed 's/\\//g')"
		fi

		if [ "${shortener}" = 'waaai' ]; then
			tempResult="$(curl -sf -F url="${uploadResult}" "${shortenerURL}")"
			shortCode="${tempResult##*short_code\":\"}"
			shortCode="${shortCode%%\"*}"
			shortenerResult="https://waa.ai/${shortCode}"
			shortenerExtension="${tempResult##*extension\":}"
			shortenerExtension="${shortenerExtension%%\}*}"
			if [ "${shortenerExtension}" ]; then
				shortenerExtension=${shortenerExtension##\"}
				shortenerExtension=${shortenerExtension%%\"}
				shortenerResult="${shortenerResult}.${shortenerExtension}"
			fi
		elif [ "${shortener}" = '0x0' ]; then
			shortenerResult="$(curl -sf -F shorten="${uploadResult}" "${shortenerURL}")"
		elif [ "${shortener}" = 'ptpb' ]; then
			shortenerResult="$(echo "${uploadResult}" | curl -sf -F c=@- "${shortenerURL}")"
		fi

		if [ "${shortener}" ]; then
			finalResult="${shortenerResult}"
		else
			finalResult="${uploadResult}"
		fi

		if [ "${?}" = 0 ]; then

			# copy to clipboard, log, and notify (unless $noComms is set)
			if [ ! "${noComms}" ]; then
					echo -n "${finalResult}" | xclip -selection primary
					echo -n "${finalResult}" | xclip -selection clipboard
					echo "$(date +"%D %R") | "${file}" | "${finalResult}"" >> ~/.uguush.log
					notify-send 'uguu~' "${finalResult}"
			fi
		fi

		echo "[${c2}OK${c0}]"
		echo "File has been uploaded: ${finalResult}"

		# if we took a screenshot, remove the temporary file
		if [ -z "${doFile}" ]; then
			rm "${localFile}"
		fi

		exit

	done
}

path() {
	if [ "${saveToPath}" ]; then
		localFilename="$(date "+%Y-%m-%d %H-%M-%S")"
		cp ${localFile} "${pathToSave}/${localFilename}.png"
	fi
}

## PARSE OPTIONS

while getopts :d:fhl:o:p:su:wxS: opt ;do
	case "${opt}" in
		d)
			# set delay value
			delaySeconds="${OPTARG}"
			case "${delaySeconds}" in
				*[!0-9]*)
					echo "Checking if delay is a number... [${c1}FAILED${c0}]" >&2
					echo 'delay is not a number.' >&2
					exit 1 ;;
			esac ;;
		f)
			# take fullscreen shot
			doFullscreen=true ;;
		h)
			# print help
			usage
			exit 0 ;;
		l)
			# set url to upload
			doURL=true
			remoteURL="${OPTARG}" ;;
		o)
			# set host
			for available in ${hosts}; do
				if [ "${OPTARG}" = "${available}" ]; then
					host="${OPTARG}"
					hostcheck=true
					break
				fi
			done
			[ "${hostcheck}" ] || exit 1 ;;
		p)
			# set path to save file
			saveToPath=true
			pathToSave="${OPTARG}"
			if [ ! -d "${pathToSave}" ]; then
				echo "Path ${pathToSave} not found, creating it..."
				mkdir -p "${pathToSave}" || exit 1
			fi ;;
		s)
			# take shot of selection
			doSelection=true ;;
		u)
			# change $file to the specified file with -u
			doFile=true
			localFile="${OPTARG}"
			if [ ! -f "${localFile}" ]; then
				echo "Checking for file... [${c1}FAILED${c0}]" >&2
				echo 'file not found.' >&2
				exit 1
			fi ;;
		w)
			# take shot of current window
			doWindow=true ;;
		x)
			# do not notify dbus, update log, or modify clipboard
			noComms=true ;;
		S)
			# set shortener
			for available in ${shorteners}; do
				if [ "${OPTARG}" = "${available}" ]; then
					shortener="${OPTARG}"
					shortenercheck=true
					break
				fi
			done
			[ "${shortenercheck}" ] || exit 1 ;;
		*)
			# print help and EXIT_FAILURE
			usage
			exit 1 ;;
	esac
done

# show usage if no arguments are given
if [ "$#" -lt 1 ]; then
	usage
	exit 1
fi

## EXECUTE FUNCTIONS

depends
delay
screenshot
path
upload

# if the program doesn't exit at the for-loop, the upload failed
echo 'File was not uploaded, did you specify a valid filename?'
